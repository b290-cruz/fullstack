let collection = [];

// Write the queue functions below.
function print() {
	return collection;
}

function enqueue(item) {
	collection[collection.length] = item
	return collection;
}

function dequeue() {
	let first = collection[0];
	for (let x=0; x < collection.length; x++) {
	    if (collection[x] === first) {
	        for (let y=x; y<(collection.length)-1; y++) {
	            collection[y] = collection[y+1];
	        }
	        collection.length = (collection.length)-1
	    }
	}

	return collection;
}

function front() {
	return collection[0];
}

function size() {
	return collection.length;
}

function isEmpty() {
	return collection.length===0;
}

module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};