console.log("meron pang time");

// Mock data, which will contain all the posts
let posts = [];
// Will be the id
let count = 1;

// CREATE (add post)
document.querySelector("#form-add-post").addEventListener("submit", e => {
	
	e.preventDefault();

	if (document.querySelector("#txt-title").value !== "" && document.querySelector("#txt-body").value !== "") {

		posts.push(
			{
				id : count,
				title : document.querySelector("#txt-title").value,
				body : document.querySelector("#txt-body").value
			}
		);

		// Next available id (an incremental value for id)
		count++;

		console.log(posts);

		showPosts(posts);
		alert("Successfully added!");

		// Clear contents upon adding
		document.querySelector("#txt-title").value = null;
		document.querySelector("#txt-body").value = null;
	} else {

		console.log("Empty values.");
		alert("Please input title and body.");
	}
})

// RETRIEVE/READ (show posts)
const showPosts = posts => {

	let postEntries = "";

	posts.forEach(post => {

		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost('${post.id}')">Edit</button>
				<button onClick="deletePost('${post.id}')">Delete</button>			
			</div>
		`
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries;
}

// Edit button function
const editPost = id => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector("#txt-edit-title").removeAttribute("disabled");
	document.querySelector("#txt-edit-body").removeAttribute("disabled");

	document.querySelector("#btn-submit-update").removeAttribute("disabled");
}

// UPDATE (update post)
document.querySelector("#form-edit-post").addEventListener("submit", e => {

	e.preventDefault();

	// Loop over the array to check for the id that is same with the element's id and text edit field
	for(let i=0; i<posts.length; i++){

		// The value posts[i].id is a Number while document.querySelector('#txt-edit-id').value is a String.
        // Therefore, it is necesary to convert the Number to a String first.
		if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value){

			// Reassign the property values
			posts[i].title = document.querySelector("#txt-edit-title").value;
			posts[i].body = document.querySelector("#txt-edit-body").value;

			console.log(posts);

			showPosts(posts);
			alert("Successfully updated!");

			// Stop the loop once changes are made
			break;
		}
	}

	// Another approach
	/*let id = document.querySelector("#txt-edit-id").value;
	posts[id - 1].title = document.querySelector("#txt-edit-title").value;
	posts[id - 1].body = document.querySelector("#txt-edit-body").value;
	showPost(posts);*/

	// Reset input fields once submitted
	document.querySelector("#txt-edit-title").value = null;
	document.querySelector("#txt-edit-body").value = null;

	// Resetting disabled attributes
	document.querySelector("#txt-edit-title").setAttribute("disabled", true);
	document.querySelector("#txt-edit-body").setAttribute("disabled", true);
	
	document.querySelector("#btn-submit-update").setAttribute("disabled", true);
})

/* READING LIST - JS REACTIVE DOM */
/*
	Directions:
	- Add a new function in script.js called deletePost()
	- This function should be able to receive the id number of the post:
		- Remove the item with the same id number from the posts array
		- Note: you can use array methods such as filter() or findIndex() and splice()
		- Then, remove the element from the DOM by first-selecting the element and using the remove() method
*/
function deletePost(id) {

	if (confirm("Are you sure you want to delete this post?") == true) {
	  for(let i=0; i<posts.length; i++){

	  	if(posts[i].id.toString() === id){

	  		posts.splice(posts[i].id, 1);

	  		count--;

	  		console.log(posts);

	  		let postDiv = document.querySelector(`#post-${id}`);
	  		postDiv.remove();

	  		alert("Post successfully deleted!");
	  		break;
	  	}
	  }
	} else {
	  // do nothing
	}

}