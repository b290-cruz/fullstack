//import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import AppNavbar from './components/AppNavbar';

// Already imported to home
//import Banner from './components/Banner';
//import Highlights from './components/Highlights';

import Courses from './pages/Courses';
import CourseView from './pages/CourseView';
import Error from './pages/Error';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';

import './App.css';
import { UserProvider } from './UserContext';

// React JS is a single page application (SPA)
// Whenever a link is clicked, it functions as if the page is being reloaded but what it actually does is it goes through the process of rendering, mounting, rerendering and unmounting components
// When a link is clicked, React JS changes the url of the application to mirror how HTML accesses its urls
// It renders the component executing the function component and it's expressions
// After rendering it mounts the component displaying the elements
// Whenever a state is updated or changes are made with React JS, it rerenders the component
// Lastly, when a different page is loaded, it unmounts the component and repeats this process
// The updating of the user interface closely mirrors that of how HTML deals with page navigation with the exception that React JS does not reload the whole page
      


// User Routes
function App() {

  // State hook for the user state that's defined here for a global scope
  // Initialized as an object with properties from the localStorage
  // This will be used to store the user information and will be used for validating if a user is logged in on the app or not
  //const [user, setUser] = useState({email : localStorage.getItem('email')});
  const [user, setUser] = useState({ id : null, isAdmin : null});

  // Function expression
  const unsetUser = () => {
    localStorage.clear();
  };

  useEffect(() => {

    if (localStorage.getItem('token')) {
      fetch("http://localhost:4000/users/details", {
          headers : {
              Authorization : `Bearer ${ localStorage.getItem('token') }`
          }
      })
      .then(res => res.json())
      .then(data => {

          console.log(data);

          // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
          if(data._id) {
            setUser({
                id: data._id,
                isAdmin : data.isAdmin
            })
          }
      })
    };

      console.log(user);
      console.log(localStorage);
  }, []);

  
  const pathname = window.location.pathname;

  return (

    // - The `BrowserRouter` component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
    // - The `Routes` component holds all our Route components. It selects which `Route` component to show based on the URL Endpoint. For example, when the `/courses` is visited in the web browser, React.js will show the `Courses` component to us.
    // - Storing information in a context object is done by providing the information using the corresponding "Provider" component and passing the information via the "value" prop

    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container fluid>
          <Routes>
            <Route path="/" element={ <Home /> } />
            <Route path="/courses" element={ <Courses /> } />
            <Route path="/courses/:courseId" element={<CourseView />} />  
            <Route path="/register" element={ <Register /> } />
            <Route path="/login" element={ <Login /> } />
            <Route path="/logout" element={ <Logout /> } />
            <Route path="/*" element={ <Error /> } />
            {/*{
              (pathname !== "/" || pathname !== "/courses" || pathname !== "/login" || pathname !== "/register" || pathname !== "/logout") ?
                <Route path="*" element={ <Banner pathway={"NotFound"}/> } />
                :
                <>
                </>
            }*/}
          </Routes>
        </Container>    
      </Router>
    </UserProvider>
  )
}

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// function App() {
//   return (

//     // We can also use <Fragment></Fragment> aside the empty tags

//       <>
//         <AppNavbar />
//         <Container>
//           {/*<Home />
//           <Courses />
//           <Register />*/}
//           <Login />
//         </Container>
//       </>
//   );
// }

export default App;

/*
  - JSX syntax requires that components should always have closing tags.
  - In the example above, since the "AppNavbar" component does not require any text to be placed in between, we add a "/" at the end to signify that they are self-closing tags. 
        
  - It's also good practice to organize importing of modules to improve code readability.
  - The example above follows the following pattern:
  - imports from built-in react modules
  - imports from downloaded packages
  - imports from user defined components
*/