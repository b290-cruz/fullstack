import { Button, Col, Row } from 'react-bootstrap';

import { Link } from 'react-router-dom';

// My solution
/*export default function Banner({ pathway }) {

	return(
		<Row>
			{
				(pathway !== "NotFound") ?
					<Col className="p-5">
						<h1>Zuitt Coding Bootcamp</h1>
						<p>Opportunities for everyone, everywhere</p>
						<Button variant="primary">Enroll Now!</Button>
					</Col>
				:
				<>
					<Col className="p-5">
						<h1>Page Not Found</h1>
						<p>Go back to the <Link style={{cursor : 'pointer', color : 'blue', textDecorationLine : 'none'}} as={Link} to="/">homepage</Link>.</p>
					</Col>
				</>

			}
		</Row>
	)
}*/

export default function Banner({ data }) {

	console.log(data);
    const { title, content, destination, label } = data;

    return (
        <Row>
            <Col>
                <h1>{ title }</h1>
                <p>{ content }</p>
                <Link className="btn btn-primary" to={ destination }>{ label }</Link>
            </Col>
        </Row>
    )
}


/*
	- The "className" prop is used in place of the "class" attribute for HTML tags in React JS due to our use of JSX elements.
*/