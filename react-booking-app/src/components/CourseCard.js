import { useState, useEffect } from 'react';
import { Button, Col, Row, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({ course }) {

	/*
	// If there is only (props) parameter. Easiest way is ({ course }) parameter
		props = {
			course : {
		        id: "wdc001",
		        name: "PHP - Laravel",
		        description: "Nostrud velit dolor excepteur ullamco consectetur aliquip tempor. Consectetur occaecat laborum exercitation sint reprehenderit irure nulla mollit. Do dolore sint deserunt quis ut sunt ad nulla est consectetur culpa. Est esse dolore nisi consequat nostrud id nostrud sint sint deserunt dolore.",
		        price: 45000,
		        onOffer: true
		    }
		}
	*/

	// Checks to see if the data was successfully passed
	console.log(course);
	// Every component receives information in a form of an object
	console.log(typeof course);

	// Descructured course variable
	const { _id, name, description, price } = course;

	// State Hook
	// Use the state hook for this component to be able to store its state
    // States are used to keep track of information related to individual components
    // Syntax
    // const [getter, setter] = useState(initialGetterValue);
    const [ count, setCount ] = useState(0);

    // Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element
    console.log(useState(0));

    // for initial onClick {enroll}
    /*function enroll() {
    	setCount(count + 1);
    	console.log(`Enrollees : ${count}`);
    }*/

    /* ACTIVITY CODE - S51*/
    // Seats state
    // const [ seats, setSeats ] = useState(30);
    // const [isOpen, setIsOpen] = useState(true);

    console.log(useState(30));

    /*function enroll() {
    	if (seats === 0) {
    		alert("No more seats.");
    	} else {
    		setCount(count + 1);
    		setSeats(seats - 1);
    		console.log(`Enrollees : ${count}`);
    	}
    }*/

   /* useEffect(() => {
        if(seats === 0){
            // alert("No more seats available");
            setIsOpen(false)
        }
    }, [seats])

    function enroll(){
            setCount(count + 1);
            setSeats(seats - 1);
    }*/


	return(
		// Answer to s50
		/*<Row>
			<Col className="pb-5">
				<Card className="p-3">
					<Card.Body>
						<Card.Title>
							<h4>Sample Course</h4>
						</Card.Title>
						<Card.Text>
							<p className="fw-bold mb-0">Description:</p>
							<p>This is a sample course offering</p>
							<p className="fw-bold mb-0">Price:</p>
							<p>PhP 40,000</p>
						</Card.Text>
						<Button variant="primary">Enroll</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>*/

		// Original card code
		/*<Card className="mb-3">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP { price }</Card.Text>
                <Card.Subtitle>Enrollees:</Card.Subtitle>
                <Card.Text>{ count } Enrollees</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>*/

        <Card className="mb-3">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP { price }</Card.Text>
                {/*<Card.Text>Enrollees: { count }</Card.Text>
                <Card.Text>Seats: { seats }</Card.Text>
                
                <Card.Text>Seats: { seats }</Card.Text>
                {
                    isOpen ?
                        <Button variant="primary" onClick={ enroll }>Enroll</Button>   
                        :
                        <Button variant="primary" onClick={ enroll } disabled>Enroll</Button>   
                }*/}
                <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
            </Card.Body>
        </Card>

	)
}