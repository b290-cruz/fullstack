import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	// Additional state hooks
	const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [mobileNo, setMobileNo] = useState('');

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');

    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    // Checker
    let isDuplicate = true;

    // Check if values are successfuly binded
    // console.log(email);
    // console.log(password1);
    // console.log(password2);

    // Function to simulate user registration
    function registerUser(e) {

    	// Prevents page redirection via form submission
    	e.preventDefault();

    	fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
    		method : "POST",
    		headers : {
    			"Content-Type" : "application/json"
    		},
    		body : JSON.stringify({
    			firstName : firstName,
			    lastName : lastName,
			    mobileNo : mobileNo,
			    email : email,
			    password : password1
    		})
    	})
    	.then(res => res.json())
    	.then(data => {

    		console.log(data)

    		if (data === true && isDuplicate === false) {

    			Swal.fire({
    			  icon: 'success',
    			  title: 'Registration successful!',
    			  text: 'Welcome to Zuitt!'
    			})

    			navigate("/login");

    			// Clear input fields
    			setFirstName('');
    			setLastName('');
    			setMobileNo('');
    			setEmail('');
    			setPassword1('');
    			setPassword2('');
    		} else {

	          Swal.fire({
	            icon: 'error',
	            title: 'Duplicate email found',
	            text: 'Please provide a different email.'
	          })
	        }

    	})

    	/*localStorage.setItem("email", email);

    	setUser({
    		email : localStorage.getItem('email')
    	})*/

    	//alert("Thank you for registering!");
    }

    useEffect(() => {
    	// Validation to enable submit button when all fields are populated and both passwords match
    	if((firstName !== "" && lastName !== "" && mobileNo !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){

    		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
	    		method : "POST",
	    		headers : {
	    			"Content-Type" : "application/json"
	    		},
	    		body : JSON.stringify({
				    email : email
	    		})
	    	})
    			.then(res => res.json())
    			.then(result => {
    				isDuplicate = result;

    				console.log("Is it a duplicate?", result);

    				setIsActive(true);
    			})

    		// setFirstName();
    		// setLastName();
    		// setMobileNo();
    		// setEmail();
    		// setPassword1();
    	} else {
    		setIsActive(false);
    	}
    }, [firstName, lastName, mobileNo, email, password1, password2]);

	return(
		(user.id) ?
			<Navigate to="/courses" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			{/*NAMES*/}
			<Form.Group controlId="userFirstName">
				<Form.Label>First  Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your first name"
					value={ firstName }
					onChange={e => setFirstName(e.target.value)}
					required
				 />
			</Form.Group>
			<Form.Group controlId="userLastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter your last name"
					value={ lastName }
					onChange={e => setLastName(e.target.value)}
					required
				 />
			</Form.Group>

			{/*MOBILE NUMBER*/}
			<Form.Group controlId="userMobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter a mobile number"
					value={ mobileNo }
					onChange={e => setMobileNo(e.target.value)}
					maxlength="11"
					required
				 />
			</Form.Group>

			{/*EMAIL*/}
			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter an email"
					value={ email }
					onChange={e => setEmail(e.target.value)}
					required
				 />
				 <Form.Text className="text-muted">We'll never share your email with anyone.</Form.Text>
			</Form.Group>

			{/*PASSWORDS*/}
			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter a password"
					value={ password1 }
					onChange={e => setPassword1(e.target.value)}
					required
				 />
			</Form.Group>
			<Form.Group controlId="password2">
				<Form.Label>Confirm Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Confirm your password"
					value={ password2 }
					onChange={e => setPassword2(e.target.value)}
					required
				 />
				 <Form.Text className="text-muted">Make sure your password are the same in order to proceed.</Form.Text>
			</Form.Group>
			
			{/*conditional rendering for submit button based on isActive state*/}
			{
				isActive ?
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2">Register</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" className="mt-2" disabled>Register</Button>
			}
		</Form>

	)
}